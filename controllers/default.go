package controllers

import (
	beego "github.com/beego/beego/v2/server/web"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"

	c.Redirect("console/dashboard.html", 302)
	return
}

func (c *MainController) Dashboard() {

	c.TplName = "views/dashboard.html"
}

func (c *MainController) Profile() {

	c.TplName = "views/profile.html"
}
