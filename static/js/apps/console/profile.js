import Vue from "vue";
import App from "./Profile.vue";
//import router from "../../router";
// import store from "../../store";

// MaterialDashboard plugin
import MaterialDashboard from "../../material-dashboard";

Vue.use(MaterialDashboard);

new Vue({
  el: '#profile',
  //store,
  //router,
  render: h => h(App)
})
