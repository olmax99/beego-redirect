import Vue from "vue";
import App from "./Dashboard.vue";
//import router from "../../router";
// import store from "../../store";

// MaterialDashboard plugin
import MaterialDashboard from "../../material-dashboard";

Vue.use(MaterialDashboard);

new Vue({
  el: '#dashboard',
  //store,
  //router,
  render: h => h(App)
})
