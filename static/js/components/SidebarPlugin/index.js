import SidebarMenu from "./SidebarMenu.vue";
import SidebarMenuBadge from "./SidebarMenuBadge.vue";
import SidebarMenuIcon from "./SidebarMenuIcon.vue";
import SidebarMenuItem from "./SidebarMenuItem.vue";
import SidebarMenuLink from "./SidebarMenuLink.vue";

const SidebarPlugin = {
  install(Vue) {
    Vue.component("sidebar-menu", SidebarMenu);
    Vue.component("sidebar-menu-badge", SidebarMenuBadge);
    Vue.component("sidebar-menu-icon", SidebarMenuIcon);
    Vue.component("sidebar-menu-item", SidebarMenuItem);
    Vue.component("sidebar-menu-link", SidebarMenuLink);
  },
};

export default SidebarPlugin;
