package main

import (
	_ "beego-redirect/routers"

	beego "github.com/beego/beego/v2/server/web"
)

func main() {
	beego.BConfig.WebConfig.TemplateLeft = "<<<"
	beego.BConfig.WebConfig.TemplateRight = ">>>"

	// NOTE: By default RequestBodies are always empty!!
	// Need this to read from Vue POSTs
	beego.BConfig.CopyRequestBody = true

	beego.SetStaticPath("/assets", "dist/assets")
	beego.SetStaticPath("/css", "dist/css")
	beego.SetStaticPath("/js", "dist/js")
	// set by webpack.*.config.js:
	// i.e htmlWebpackPlugin: 'public/index.html'
	beego.BConfig.WebConfig.ViewsPath = "dist"
	beego.Run()
}
