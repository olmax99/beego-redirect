package routers

import (
	"beego-redirect/controllers"

	beego "github.com/beego/beego/v2/server/web"
)

func init() {
	beego.Router("/console", &controllers.MainController{})
	beego.Router("/console/dashboard", &controllers.MainController{}, "get:Dashboard")
	beego.Router("/console/profile", &controllers.MainController{}, "get:Profile")
}
