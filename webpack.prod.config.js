const { VueLoaderPlugin } = require("vue-loader");
const htmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const autoprefixer = require("autoprefixer");
const path = require("path");
const projectRoot = path.resolve(__dirname);

module.exports = {
  mode: "development",
  devtool: "source-map",
  entry: {
    dashboard: "./static/js/apps/console/dashboard.js",
    profile: "./static/js/apps/console/profile.js"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "js/[name].[hash:8].js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.vue$/,
        loader: "vue-loader"
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              plugins: () => [autoprefixer()]
            }
          },
          "sass-loader",
        ]
      },
      {
        test: /\.(png|jpe?g|gif|webm|mp4|svg)$/,
        loader: "file-loader",
        options: {
          name: "img/[name][contenthash:8].bundle.[ext]",
          outputPath: "assets",
          esModule: false
        }
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "css/[name].[contenthash:8].css"
    }),
    new htmlWebpackPlugin({
      filename: "views/dashboard.html", // target location
      template: path.resolve(__dirname, "views/console/dashboard.html"), // source location
      chunks: ["dashboard"], // allows to select only specific chunks as defined in entry
      favicon: path.resolve(__dirname, "static/assets/favicon.ico"),
      title: "Beego-Re" // define tab name
    }),
    new htmlWebpackPlugin({
      filename: "views/profile.html", // target location
      template: path.resolve(__dirname, "views/console/profile.html"), // source location
      chunks: ["profile"], // allows to select only specific chunks as defined in entry
      favicon: path.resolve(__dirname, "static/assets/favicon.ico"),
      title: "Beego-Re" // define tab name
    }),
  ],
  resolve: {
    alias: {
      vue$: "vue/dist/vue.runtime.esm.js",
      "@": path.join(__dirname, "./")
    },
    extensions: ["*", ".js", ".vue", ".json"]
  },
  optimization: {
    moduleIds: "hashed",
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          priority: -10,
          chunks: "all"
        }
      }
    }
  }
};
